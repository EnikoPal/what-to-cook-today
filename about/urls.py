from django.urls import path

from about import views

urlpatterns = [
    path('', views.about_page_view, name='about_page'),

]