from django import forms
from django.forms import TextInput, EmailInput

from about.models import Contact


class ContactForm(forms.ModelForm):

    class Meta:
        model = Contact
        fields = '__all__'
        widgets = {
            'name': TextInput(attrs={'placeholder': 'Please enter your name', 'class': 'form-control'}),
            'organization': TextInput(attrs={'placeholder': 'Please enter organization name', 'class': 'form-control'}),
            'email': EmailInput(attrs={'placeholder': 'Please enter your email ', 'class': 'form-control'}),
            'comment': TextInput(attrs={'placeholder': 'Please enter your message', 'class': 'form-control'}),

        }