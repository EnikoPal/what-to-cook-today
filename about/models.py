from django.db import models


# Create your models here.
class About(models.Model):
    description = models.TextField(max_length=300)
    category = models.CharField(max_length=255)

    def __str__(self):
        return self.description




class Contact(models.Model):
    name = models.CharField(max_length=250)
    organization = models.CharField(max_length=400)
    email = models.EmailField()
    comment = models.TextField()

    def __str__(self):
        return f'Contact from {self.name}'

