from django.urls import reverse_lazy
from about.forms import ContactForm
from django.shortcuts import render, redirect


def about_page_view(request):
    print('about_page_view')
    form = ContactForm()
    if request.method == 'POST':
        print('about_page_view POST')
        form = ContactForm(request.POST)
        if form.is_valid():
            print('about_page_view form.is_valid()')
            contact = form.save()
            return redirect(reverse_lazy('about_page'))

    return render(request, "about/aboutpage.html", {'form': form})










