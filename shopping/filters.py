import django_filters
from shopping.models import Store


class StoreFilters(django_filters.FilterSet):
    # list_of_stores = [Store.name]
    # name = django_filters.ChoiceFilter(choices=list(set(list_of_stores)))
    name = django_filters.CharFilter(lookup_expr='icontains', label='name')
    location = django_filters.CharFilter(lookup_expr='icontains', label='location')
    category = django_filters.CharFilter(lookup_expr='icontains', label='category')
    ingredients = django_filters.CharFilter(lookup_expr='icontains', label='ingredients')

    class Meta:
        model = Store
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
