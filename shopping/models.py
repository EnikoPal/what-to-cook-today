from django.db import models


class Store(models.Model):
    name = models.CharField(max_length=200)
    location = models.TextField(max_length=200)
    category = models.CharField(max_length=200)
    ingredients = models.CharField(max_length=270)

    def __str__(self):
        return f'{self.name}'


class ShoppingList(models.Model):
    category = models.CharField(max_length=200)
    ingredient = models.CharField(max_length=270)

    def __str__(self):
        return f'{self.category} {self.ingredient}'
