from django.contrib.auth.mixins import PermissionRequiredMixin, LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView, DetailView
from shopping.filters import StoreFilters
from shopping.forms import StoreForm, StoreUpdateForm
from shopping.models import Store, ShoppingList


class StoreCreateView(PermissionRequiredMixin, CreateView):
    template_name = 'shopping/create_store.html'
    model = Store
    form_class = StoreForm
    success_url = reverse_lazy('about_page')
    permission_required = 'shopping.add_store'


class StoreListView(ListView):
    template_name = 'shopping/list_of_stores.html'
    model = Store
    context_object_name = 'all_stores'

    def get_queryset(self):
        return Store.objects.filter()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        my_filters = StoreFilters(self.request.GET, queryset=Store.objects.filter())
        get_all_stores = my_filters.qs
        context['all_stores'] = get_all_stores
        context['form_filters'] = my_filters.form
        return context


class StoreUpdateView(PermissionRequiredMixin, UpdateView):
    template_name = 'shopping/update_store.html'
    model = Store
    form_class = StoreUpdateForm
    success_url = reverse_lazy('list-of-stores')
    permission_required = 'store.change_store'


class StoreDeleteView(PermissionRequiredMixin, DeleteView):
    template_name = 'shopping/delete_store.html'
    model = Store
    success_url = reverse_lazy('list-of-stores')
    permission_required = 'store.delete_store'


class StoreDetailView(LoginRequiredMixin,  DetailView):
    template_name = 'shopping/details_store.html'
    model = Store
    success_url = reverse_lazy('list-of-stores')


class ShoppingListView(ListView):
    template_name = 'shopping/list_of_stores.html'
    model = ShoppingList
    context_object_name = 'all_stores'
