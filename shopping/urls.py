from django.urls import path
from shopping import views

urlpatterns = [
    path('create_store/', views.StoreCreateView.as_view(), name='create-store'),
    path('list_of_stores/', views.StoreListView.as_view(), name='list-of-stores'),
    path('shopping_list/', views.ShoppingListView.as_view(), name='shopping-list'),

    path('update_store/<int:pk>/', views.StoreUpdateView.as_view(), name='update-store'),
    path('delete_store/<int:pk>/', views.StoreDeleteView.as_view(), name='delete-store'),
    path('details_store/<int:pk>/', views.StoreDetailView.as_view(), name='details-store'),

]
