from django import forms
from django.forms import TextInput
from shopping.models import Store


class StoreForm(forms.ModelForm):
    class Meta:
        model = Store
        fields = '__all__'

        widgets = {
            'name': TextInput(attrs={'placeholder': 'Please enter store name', 'class': 'form-control'}),
            'location': TextInput(attrs={'placeholder': 'Please enter store location', 'class': 'form-control'}),
            'category': TextInput(attrs={'placeholder': 'Please enter store category', 'class': 'form-control'}),
            'ingredients': TextInput(attrs={'placeholder': 'Please enter the ingredient', 'class': 'form-control'}),
        }


class StoreUpdateForm(forms.ModelForm):
    class Meta:
        model = Store
        fields = '__all__'

        widgets = {
            'name': TextInput(attrs={'placeholder': 'Please enter new store name', 'class': 'form-control'}),
            'location': TextInput(attrs={'placeholder': 'Please enter store new location', 'class': 'form-control'}),
            'category': TextInput(attrs={'placeholder': 'Please enter store new category', 'class': 'form-control'}),

        }
