from django.db import models


class Recipie(models.Model):
    name = models.CharField(max_length=30)
    category = models.CharField(max_length=30)
    ingredients = models.CharField(max_length=270)
    main_ingredients = models.CharField(max_length=270)
    description = models.TextField(max_length=300)
    active = models.BooleanField(default=True)

    def __str__(self):
        return f'{self.name} {self.category}'
