from django import forms
from django.forms import TextInput
from recipies.models import Recipie


class RecipieForm(forms.ModelForm):
    class Meta:
        model = Recipie
        fields = '__all__'

    widgets = {
            'name': TextInput(attrs={'placeholder': 'Please enter recipie name', 'class': 'form-control'}),
            'category': TextInput(attrs={'placeholder': 'Please enter recipie category ', 'class': 'form-control'}),
            'ingredients': TextInput(attrs={'placeholder': 'Please enter the ingredients', 'class': 'form-control'}),
            'description': TextInput(attrs={'placeholder': 'Please enter description', 'class': 'form-control'}),

        }


class RecipieUpdateForm(forms.ModelForm):
    class Meta:
        model = Recipie
        fields = '__all__'

        widgets = {
            'name': TextInput(attrs={'placeholder': 'Please enter recipie new name', 'class': 'form-control'}),
            'category': TextInput(attrs={'placeholder': 'Please enter recipie new category ', 'class': 'form-control'}),
            'ingredients': TextInput(attrs={'placeholder': 'Please enter the ingredients', 'class': 'form-control'}),
            'description': TextInput(attrs={'placeholder': 'Please enter new description', 'class': 'form-control'}),
        }
