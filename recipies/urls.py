from django.urls import path
from recipies import views


urlpatterns = [
    path('recipies/create_recipie/', views.RecipieCreateView.as_view(), name='create-recipie'),
    path('recipies/list_of_recipies/', views.RecipieListView.as_view(), name='list-of-recipies'),
    path('update_recipie/<int:pk>/', views.RecipieUpdateView.as_view(), name='update-recipie'),
    path('delete_recipie/<int:pk>/', views.RecipieDeleteView.as_view(), name='delete-recipie'),
    path('details_recipie/<int:pk>/', views.RecipieDetailView.as_view(), name='details-recipie'),
]