import django_filters
from recipies.models import Recipie


class RecipieFilters(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr='icontains')
    category = django_filters.CharFilter(lookup_expr='icontains')
    ingredients = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Recipie
        fields = ['name', 'category', 'ingredients']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.filters['name'].field.widget.attrs.update(
            {'class': 'form-control', 'placeholder': 'Please enter recipe name'})
        self.filters['category'].field.widget.attrs.update(
            {'class': 'form-control', 'placeholder': 'Please enter recipe category'})
        self.filters['ingredients'].field.widget.attrs.update(
            {'class': 'form-control', 'placeholder': 'Please enter recipe ingredients'})
