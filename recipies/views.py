from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, DetailView, DeleteView, UpdateView
from recipies.forms import RecipieForm, RecipieUpdateForm
from recipies.models import Recipie
from recipies.filters import RecipieFilters


class RecipieCreateView(SuccessMessageMixin, CreateView, PermissionRequiredMixin):
    template_name = 'recipies/create_recipie.html'
    model = Recipie
    form_class = RecipieForm
    success_url = reverse_lazy('about_page')
    success_message = 'Congratulations!{name} was successfully added'
    permission_required = 'recipies.add_recipie'


class RecipieListView(CreateView):
    template_name = "recipies/list_of_recipies.html"
    model = Recipie
    context_object_name = 'all_recipies'
    fields = "__all__"

    def get_queryset(self):
        return Recipie.objects.filter(active=True)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        my_filters = RecipieFilters(self.request.GET, queryset=Recipie.objects.filter(active=True))
        get_all_recipies = my_filters.qs

        context['all_recipies'] = get_all_recipies
        context['form_filters'] = my_filters.form
        return context


# Auxiliary method to check if a specific ingredient is found in the list of ingredients
def search_ingredient(searchword, ingredientslist):
    ingredientsTokens = ingredientslist.split()
    for item in ingredientsTokens:
        if item == searchword:
            return True
    return False


class RecipieUpdateView(UpdateView, PermissionRequiredMixin):
    template_name = 'recipies/update_recipie.html'
    model = Recipie
    form_class = RecipieUpdateForm
    success_url = reverse_lazy('list-of-recipies')
    permission_required = 'recipie.change_recipie'


class RecipieDeleteView(DeleteView, PermissionRequiredMixin):
    template_name = 'recipies/delete_recipie.html'
    model = Recipie
    success_url = reverse_lazy('list-of-recipies')
    permission_required = 'recipie.delete_recipie'


class RecipieDetailView(DetailView):
    template_name = 'recipies/details_recipie.html'
    model = Recipie
