Login page to app admin:  
http://whatscooking.pythonanywhere.com/admin/login/?next=/admin/  
  
Path to pythonanywhere dashboard:  
https://www.pythonanywhere.com/user/whatscooking/  
  
Path to web admin page:  
https://www.pythonanywhere.com/user/whatscooking/webapps/#tab_id_whatscooking_pythonanywhere_com  
  
Path to files on web server:  
https://www.pythonanywhere.com/user/whatscooking/files/home/whatscooking  
  
Path to bash console:  
https://www.pythonanywhere.com/user/whatscooking/consoles/29747951/  
!!! change directory to 'what-to-cook-today' to access git repository  
  
to get latest sources from git repository:  
git pull  
  
to save web db changes in server stash:  
git stash -m 'my message'  
  