from datetime import datetime
from random import randint
from django.contrib.auth.models import User
from django.core.mail import EmailMessage
from django.template.loader import get_template
from django.urls import reverse_lazy
from django.views.generic import CreateView
from What_to_cook_today.settings import EMAIL_HOST_USER
from userextend.forms import UserForm
from userextend.models import History


class UserCreateView(CreateView):
    template_name = 'userextend/create_user.html'
    model = User
    form_class = UserForm
    success_url = reverse_lazy('login')

    def form_valid(self, form):
        if form.is_valid():
            new_user = form.save(commit=False)
            new_user.first_name = new_user.first_name.title()
            new_user.last_name = new_user.last_name.title()
            new_user.username = f'{new_user.first_name[0].lower()}' \
                                f'{new_user.last_name.lower().replace(" ", "")}_{randint(100000, 999999)}'
            new_user.save()

            history_text = f'Userul a fost adaugat la data de {datetime.now()}.Firstname {new_user.username},' \
                           f'Last_name: {new_user.last_name}, Email: {new_user.email}, Username: {new_user.username}.'

            History.objects.create(text=history_text, created_at=datetime.now())

            details_user = {
                'fullname': f'{new_user.first_name} {new_user.last_name}',
                'username': new_user.username

            }
            subject = 'Confirmare cont nou in aplicatie'
            message = get_template('userextend/mail.html').render(details_user)

            mail = EmailMessage(subject, message, EMAIL_HOST_USER, [new_user.email])
            mail.content_subtype = 'html'
            mail.send()

        return super().form_valid(form)
