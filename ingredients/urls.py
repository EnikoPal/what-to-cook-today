from django.urls import path
from ingredients import views


urlpatterns = [
    path('ingredients/create_ingredient/', views.IngredientCreateView.as_view(), name='create-ingredient'),
    path('ingredients/list_of_ingredients/', views.IngredientListView.as_view(), name='list-of-ingredients'),
    path('update_ingredient/<int:pk>/', views.IngredientUpdateView.as_view(), name='update-ingredient'),
    path('delete_ingredient/<int:pk>/', views.IngredientDeleteView.as_view(), name='delete-ingredient'),
    path('details_ingredient/<int:pk>/', views.IngredientDetailView.as_view(), name='details-ingredient'),
    path('ingredients/grocery_list/', views.GroceryList.as_view(), name='grocery-list'),
    path('add_to_grocery_list/<int:pk>/', views.GroceryList.add_to_grocery_list, name='add-ingredient-to-grocery-list'),
    path('clear_grocery_list_item/<str:prodName>/', views.GroceryList.clear_item, name='clear-grocery-list-item'),
    path('clear_grocery_list/', views.GroceryList.clear_grocery_list, name='clear-grocery-list'),
    path('save_grocery_list/<int:userID>/<str:userName>/', views.GroceryList.save_grocery_list,
         name='save-grocery-list'),
    path('load_grocery_list/<int:userID>/', views.GroceryList.load_grocery_list, name='load-grocery-list'),
    path('search/', views.search, name='search'),
]
