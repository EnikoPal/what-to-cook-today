import django_filters
from ingredients.models import Ingredient


class IngredientFilters(django_filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr='icontains', label='name')
    category = django_filters.CharFilter(lookup_expr='icontains', label='category')

    class Meta:
        model = Ingredient
        fields = ['name', 'category']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.filters['name'].field.widget.attrs.update(
            {'class': 'form-control', 'placeholder': 'Please enter ingredient name'})
        self.filters['category'].field.widget.attrs.update(
            {'class': 'form-control', 'placeholder': 'Please enter ingredient category category'})

