from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.db.models import Q
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView, DeleteView, DetailView
from ingredients.forms import IngredientForm, IngredientUpdateForm
from ingredients.models import Ingredient, Grocery, UserItemList
from ingredients.filters import IngredientFilters
from ingredients.models import UserShoppingList


class IngredientCreateView(LoginRequiredMixin, SuccessMessageMixin, CreateView, PermissionRequiredMixin):
    template_name = 'ingredients/create_ingredient.html'
    model = Ingredient
    form_class = IngredientForm
    success_url = reverse_lazy('list-of-ingredients')
    success_message = 'Congratulations! Item was successfully added'
    permission_required = 'ingredient.add_ingredient'

    def get_success_message(self, cleaned_data):
        return self.success_message.format(name=self.object.name, category=self.object.category)


class IngredientListView(CreateView):
    template_name = 'ingredients/list_of_ingredients.html'
    model = Ingredient
    context_object_name = 'all_ingredients'
    fields = "__all__"

    def get_queryset(self):
        return Ingredient.objects.filter

    def get_context_data(self, get_all_ingredients=None, **kwargs):
        context = super().get_context_data(**kwargs)
        # search
        my_filters = IngredientFilters(self.request.GET)
        get_all_ingredients = my_filters.qs

        context['all_ingredients'] = get_all_ingredients
        return context


def search(request):
    query = request.GET.get('q', '')
    ingredients = Ingredient.objects.filter(Q(name__icontains=query) | Q(category__icontains=query))
    return render(request, 'ingredients/search.html', {'ingredients': ingredients})


class IngredientUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = 'ingredients/update_ingredient.html'
    model = Ingredient
    form_class = IngredientUpdateForm
    success_url = reverse_lazy('list-of-ingredients')
    permission_required = 'ingredients.change_ingredient'


class IngredientDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = 'ingredients/delete_ingredient.html'
    model = Ingredient
    success_url = reverse_lazy('list-of-ingredients')
    permission_required = 'ingredient.delete_ingredient'


class IngredientDetailView(DetailView):
    template_name = 'ingredients/details_ingredient.html'
    model = Ingredient


class GroceryList(LoginRequiredMixin, CreateView):
    template_name = 'ingredients/grocery_list.html'
    model = Grocery
    context_object_name = 'all_ingredients'
    permission_required = 'ingredient.view_grocery_list'
    fields = "__all__"
    success_url = reverse_lazy('list-of-ingredients')


    shoppingList = []
    def add_to_grocery_list(self, pk):
        # Add current ingredient into the list
        ingredient = Ingredient.objects.get(pk=pk)
        GroceryList.shoppingList.append(ingredient.name)

        for item in GroceryList.shoppingList:
            return redirect('list-of-ingredients')

    def clear_item(self, prodName):
        GroceryList.shoppingList.remove(prodName)

        return redirect('grocery-list')

    def clear_grocery_list(self):
        GroceryList.shoppingList.clear()
        return redirect('list-of-ingredients')

    def get(self, request):
        return render(request, self.template_name, {"shoppingList": GroceryList.shoppingList})

    def save_grocery_list(self, userID, userName):
        content = UserShoppingList(userID, userName, GroceryList.shoppingList)
        content.save()
        return redirect('grocery-list')

    def load_grocery_list(self, userID):
        content = UserShoppingList.objects.get(pk=userID)
        # a working approach, remove extra characters and split the string into tokens
        myList = content.shoppingList
        myList = myList.replace("[", "")
        myList = myList.replace("]", "")
        myList = myList.replace("'", "")
        myList = myList.replace(",", "")

        for item in myList.split():
            print(item)

        GroceryList.shoppingList = myList.split()
        return redirect('grocery-list')


@login_required
def save_list(request):
    if request.method == 'POST':
        item_list = request.POST.getlist('item_list')
        item_list_str = ','.join(item_list)

        # Get the user and create or update their item list
        user_item_list, created = UserItemList.objects.get_or_create(user=request.user)
        user_item_list.item_list = item_list_str
        user_item_list.save()

        return redirect('some_view')  # Redirect to some other view after saving the list

    return render(request, 'save_list.html')


@login_required
def view_list(request):
    user_item_list = UserItemList.objects.filter(user=request.user).first()
    if user_item_list:
        item_list = user_item_list.item_list.split(',')
    else:
        item_list = []

    return render(request, 'view_list.html', {'item_list': item_list})


