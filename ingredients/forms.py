from django import forms
from django.forms import TextInput
import django_filters
from ingredients.models import Ingredient, Grocery, Property


class IngredientForm(forms.ModelForm):
    class Meta:
        model = Ingredient
        fields = "__all__"

        widgets = {
            'name': TextInput(attrs={'placeholder': 'Please enter ingredient name', 'class': 'form-control'}),
            'category': TextInput(attrs={'placeholder': 'Please enter ingredient category', 'class': 'form-control'}),
            'description': TextInput(attrs={'placeholder': 'Please enter ingredient description', 'class': 'form-control'}),
        }

    def clean(self):
        cleaned_data = self.cleaned_data
        check_name = Ingredient.objects.filter(name=cleaned_data['name'])
        if check_name:
            msg = 'This ingredient already exists in db'
            self._errors['name'] = self.error_class([msg])

        return cleaned_data


class IngredientUpdateForm(forms.ModelForm):
    class Meta:
        model = Ingredient
        fields = '__all__'

        widgets = {
            'name': TextInput(attrs={'placeholder': 'Please enter ingredient name', 'class': 'form-control'}),
            'category': TextInput(attrs={'placeholder': 'Please enter ingredient category', 'class': 'form-control'}),
            'description': TextInput(attrs={'placeholder': 'Please enter ingredient description', 'class': 'form-control'}),
        }


class GroceryForm(forms.ModelForm):
    class Meta:
        model = Grocery
        fields = "__all__"



class SearchPropertyForm(forms.Form):
    name = forms.CharField(max_length=30, widget= TextInput(attrs={'placeholder': 'Please enter ingredient name', 'class': 'form-control'}))
    category = forms.CharField(max_length=30, widget= TextInput(attrs={'placeholder': 'Please enter ingredient category', 'class': 'form-control'}))



class PropertyFilter(django_filters.FilterSet):
    class Meta:
        model = Property
        fields = "__all__"


class IngredientFilters(forms.Form):
    class Meta:
        model = Ingredient
        fields = "__all__"
        widgets = {
            'name': TextInput(attrs={'placeholder': 'Please enter ingredient name', 'class': 'form-control'}),
            'category': TextInput(attrs={'placeholder': 'Please enter ingredient category', 'class': 'form-control'}),
        }




