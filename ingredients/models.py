from django.contrib.auth.models import User
from django.db import models


class Ingredient(models.Model):
    name = models.CharField(max_length=30)
    category = models.CharField(max_length=30)
    description = models.TextField(max_length=270)

    def __str__(self):
        return f'{self.name} {self.category} {self.description}'


class Grocery(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return f'{self.name}'


class UserShoppingList(models.Model):
    id = models.indexes
    user_id = models.IntegerField
    username = models.CharField(max_length=100)
    shoppingList = models.CharField(max_length=255)

    def __str__(self):
        return f'{self.id} {self.user_id} {self.username} {self.shoppingList}'


class UserItemList(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)


class Property(models.Model):
    class Meta:
        verbose_name_plural = "properties"

    name = models.CharField(max_length=30)
    category = models.CharField(max_length=30)

    def __unicode__(self):
        return self.name
